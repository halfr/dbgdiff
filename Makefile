SRC = a.c

CFLAGS = -frandom-seed=42

TARGET = $(SRC:.c=.nodbg) $(SRC:.c=.g) $(SRC:.c=.ggdb) $(SRC:.c=.g3) $(SRC:.c=.ggdb3)

.PHONY: all
all: $(TARGET)

.PHONY: clean
clean:
	$(RM) $(TARGET) $(addsuffix .dbgdump, $(TARGET))

COMPILE = $(CC) $(CFLAGS) -o $@ $^
DBGDUMP = readelf --debug-dump $@ > $@.dbgdump

%.nodbg: %.c
	$(COMPILE)
	$(DBGDUMP)

%.g: %.c
	$(COMPILE) -g
	$(DBGDUMP)

%.ggdb: %.c
	$(COMPILE) -ggdb
	$(DBGDUMP)

%.g3: %.c
	$(COMPILE) -g3
	$(DBGDUMP)

%.ggdb3: %.c
	$(COMPILE) -ggdb3
	$(DBGDUMP)
